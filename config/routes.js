/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': {
    view: 'homepage'
  },

  // '/scheduler': {
  //   view: 'scheduler'
  // },

  'POST /mde'      : function (req,res) {
    console.log("GET BODY", req.body)
    ElasticSearch.getRecords(false, req.body)
      .then( items => {
         res.json(items);
      });
  },
  
  'GET /settings'      : function (req,res) {
    SettingsService.getSettings(function(data) {
      res.json(data)
    });
  },

  'POST /settings/update' : function (req,res) {
    SettingsService.updateSettings(req.body, function(data) {
      res.json(data)
    });
  },

  'GET /contracts' : function(req, res) {
    console.log("requested contracts");
    NexusService.getContracts(function(contracts) {
      res.json(contracts);
    })
  },

  'POST /contract/template' : function (req,res) {
    console.log("POST TO /contract/template");
    NexusService.getContractTemplate(req.body, function(data) {
      res.json(data)
    });
  },

  'GET /groups' : function(req, res) {
    console.log("requested groups");
    NexusService.getGroups(function(groups) {
      res.json(groups);
    })
  },

  'GET /groupUsers' : function(req, res) {
    console.log("requested group users");
    NexusService.getGroupUsers(function(groupUsers) {
      res.json(groupUsers);
    })
  },

  'GET /communities' : function(req, res) {
    console.log("requested communities");
    NexusService.getCommunities(function(communities) {
      res.json(communities);
    })
  },

  'GET /communityConcat' : function(req, res) {
    console.log("requested community concat");
    NexusService.getCommunityConcat(function(communityConcat) {
      res.json(communityConcat);
    })
  }, 

  'GET /members' : function(req, res) {
    console.log("requested members");
    NexusService.getMembers(function(members) {
      res.json(members);
    })
  }, 

  'GET /users' : function(req, res) {
    console.log("requested users");
    NexusService.getUsers(function(users) {
      res.json(users);
    })
  }, 

  'GET /providers' : function (req,res) {
    console.log("requested products");
    NexusService.getUnregulatedUsers(function(providers) {
      res.json(providers);
    })
  }

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
