
'use strict'

const request             = require('request');
const request_timeout     = 45 * 1000;
const xml                 = require("xml-parse");

const Web3	 	            = require('web3');

let web3;

let comChainContract;


//let schedule = require('node-schedule');
//schedule.scheduleJob

const config   = {
	'kirk' 		: {
		'host'		: '192.168.3.66',
		'port' 		: '7070',
		'https' 	: false,
	},
    'prefix'    : '/restapi/casapi',
	'actions' 	: {
		'makecall'	: '/callcontrol/makecall/'
	},
    'destination' : '200124'
};

const serviceName = 'kirk';

const scheduleTimeout = 1000 * 10//60;// * 60 * 24;

//http://192.168.3.66:7070/restapi/casapi/callcontrol/makecall/priyanka@cluster.gltd.local%2Foffice/200124?interest=C3014Test-ptSEP74DE2B73BBE8
//http://192.168.3.66:7070/restapi/casapi/callcontrol/makecall/demouser0@cluster.gltd.local/vraudit/200124
/*
body {"iq": {
   "from": "ciscodefault.cluster.gltd.local",
   "id": "url-1510253738466",
   "to": "admin@cluster.gltd.local/casrest ",
   "type": "result",
   "command": {
      "node": "http://xmpp.org/protocol/openlink:01:00:00#make-call",
      "note": {
         "type": "error",
         "content": "MakeCall Error Make call failed"
      },
      "xmlns": "http://jabber.org/protocol/commands",
      "status": "completed"
   }
}}
*/

/*
body {"iq": {
   "from": "ciscodefault.cluster.gltd.local",
   "id": "url-1510257051083",
   "to": "admin@cluster.gltd.local/casrest ",
   "type": "result",
   "command": {
      "node": "http://xmpp.org/protocol/openlink:01:00:00#make-call",
      "xmlns": "http://jabber.org/protocol/commands",
      "iodata": {
         "xmlns": "urn:xmpp:tmp:io-data",
         "type": "output",
         "out": {"callstatus": {
            "call": {
               "duration": 2,
               "caller": {
                  "number": 3013,
                  "name": 3013
               },
               "originator-ref": {
                  "xmlns": "http://xmpp.org/protocol/openlink:01:00:00#originator-ref",
                  "property": [
                     {
                        "id": "universal-callid",
                        "value": 16868135
                     },
                     {
                        "id": "gcid",
                        "value": "90919/1"
                     }
                  ]
               },
               "called": {
                  "number": 200124,
                  "name": 200124
               },
               "interest": "C3013Test-ptSEP003094C3E049",
               "profile": "SEP003094C3E049",
               "id": "C3013Test-ptSEP003094C3E049-16868135",
               "state": "CallOriginated",
               "actions": {
                  "ClearConnection": "",
                  "SendDigits": ""
               },
               "direction": "Outgoing",
               "participants": {"participant": {
                  "duration": 2,
                  "number": 3013,
                  "jid": "nigels@cluster.gltd.local",
                  "type": "Active",
                  "category": "Owner",
                  "direction": "Outgoing",
                  "timestamp": "Thu Nov 09 19:50:51 GMT 2017"
               }}
            },
            "xmlns": "http://xmpp.org/protocol/openlink:01:00:00#call-status",
            "busy": false
         }}
      },
      "status": "completed"
   }
}}

*/

//var c = 0;
const schedule = (timeout, contractAddress, action, jid, destination, interest) => setInterval( () => {
	/*if (c > 0)
        return;
    c++;*/
    //const action = 'makecall'
    console.log('contractAddress, action, jid, destination, interest');
    console.log(contractAddress, action, jid, destination, interest);

    return
    
    let auth = "Basic " + new Buffer('admin' + ":" + 'Pa55w0rd').toString("base64");

    let url = 'http://'
    if (config[serviceName].https === true)
        url = 'https://';

    url += config[serviceName].host + ":" + config[serviceName].port + config.prefix + config.actions[action] + jid + "/" + destination;

    request.get( url, {
        timeout: request_timeout,
        rejectUnauthorized: false,
        headers : {
            "Authorization" : auth
        }
    }, (error, api_response, body) => {
        if (error)
            return console.log(error);
        
        //body.iq.command
        console.log('body',body);

        let auditJson = {
            vrauditcallid       : '',
            vrauditmessage      : '',
            vraudittimestamp    : new Date()
        };
        //console.log('api_response', api_response);
    });




}, timeout)

const getClearString  = function(value, toAscii) {

    if (toAscii)
        value =  web3.toAscii(value);

    let str = "";
    for (let i = 0; i < value.length; i++){
       if (value.charCodeAt(i) > 0)
        str += value[i];
    };

    return str;

};
/*
    vrauditcallid
    vrauditmessage
    vraudittimestamp
*/
const getComChainContracts = () => {

	const abi                 		= [{"constant":false,"inputs":[{"name":"_identifier","type":"bytes32"}],"name":"setIdentifier","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function","stateMutability":"nonpayable"},{"constant":false,"inputs":[{"name":"_storagePeriod","type":"uint8"},{"name":"_refreshPeriod","type":"uint8"}],"name":"createMiFiD2Template","outputs":[],"payable":false,"type":"function","stateMutability":"nonpayable"},{"constant":true,"inputs":[],"name":"getOriginator","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function","stateMutability":"view"},{"constant":true,"inputs":[],"name":"getMiFiD2Address","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function","stateMutability":"view"},{"constant":false,"inputs":[{"name":"_link","type":"address"}],"name":"removeLink","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function","stateMutability":"nonpayable"},{"constant":false,"inputs":[{"name":"_hash","type":"string"},{"name":"_location","type":"string"}],"name":"pushMetaData","outputs":[{"name":"_index","type":"uint256"}],"payable":false,"type":"function","stateMutability":"nonpayable"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"type":"function","stateMutability":"nonpayable"},{"constant":true,"inputs":[{"name":"_key","type":"bytes32"}],"name":"getProperty","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function","stateMutability":"view"},{"constant":true,"inputs":[{"name":"_for","type":"address"}],"name":"checkAccess","outputs":[{"name":"mdeAccess","type":"uint8"},{"name":"contractAccess","type":"uint8"}],"payable":false,"type":"function","stateMutability":"view"},{"constant":true,"inputs":[],"name":"version","outputs":[{"name":"","type":"uint8"}],"payable":false,"type":"function","stateMutability":"view"},{"constant":false,"inputs":[{"name":"_address","type":"address"},{"name":"_access","type":"uint8"}],"name":"setContractAccess","outputs":[],"payable":false,"type":"function","stateMutability":"nonpayable"},{"constant":true,"inputs":[],"name":"getOwner","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function","stateMutability":"view"},{"constant":false,"inputs":[{"name":"_address","type":"address"},{"name":"_access","type":"uint8"}],"name":"setMDEAccess","outputs":[],"payable":false,"type":"function","stateMutability":"nonpayable"},{"constant":true,"inputs":[],"name":"getPropertiesKeys","outputs":[{"name":"","type":"bytes32[]"}],"payable":false,"type":"function","stateMutability":"view"},{"constant":false,"inputs":[{"name":"_link","type":"address"}],"name":"addLink","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function","stateMutability":"nonpayable"},{"constant":false,"inputs":[{"name":"_newOwner","type":"address"}],"name":"changeOwner","outputs":[{"name":"oldOwner","type":"address"}],"payable":false,"type":"function","stateMutability":"nonpayable"},{"constant":true,"inputs":[],"name":"getIdentifier","outputs":[{"name":"","type":"bytes32"}],"payable":false,"type":"function","stateMutability":"view"},{"constant":false,"inputs":[{"name":"_key","type":"bytes32"},{"name":"_val","type":"string"}],"name":"addProperty","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function","stateMutability":"nonpayable"},{"constant":true,"inputs":[{"name":"_sender","type":"address"}],"name":"pullMetaData","outputs":[{"name":"","type":"string"},{"name":"","type":"string"}],"payable":false,"type":"function","stateMutability":"view"},{"constant":true,"inputs":[{"name":"_sender","type":"address"},{"name":"_index","type":"uint256"}],"name":"getMetaData","outputs":[{"name":"","type":"string"},{"name":"","type":"string"}],"payable":false,"type":"function","stateMutability":"view"},{"constant":true,"inputs":[],"name":"getLinks","outputs":[{"name":"","type":"address[]"}],"payable":false,"type":"function","stateMutability":"view"},{"constant":true,"inputs":[],"name":"getMetaDataKeys","outputs":[{"name":"","type":"address[]"}],"payable":false,"type":"function","stateMutability":"view"},{"inputs":[{"name":"_owner","type":"address"},{"name":"_identifier","type":"bytes32"},{"name":"_mifid2","type":"bool"}],"payable":false,"type":"constructor","stateMutability":"nonpayable"},{"anonymous":false,"inputs":[{"indexed":false,"name":"owner","type":"address"},{"indexed":false,"name":"identifier","type":"bytes32"}],"name":"NewComChain","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"sender","type":"address"},{"indexed":false,"name":"hash","type":"string"},{"indexed":false,"name":"index","type":"uint256"}],"name":"NewReference","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"key","type":"bytes32"}],"name":"NewProperty","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"link","type":"address"}],"name":"NewLink","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"link","type":"address"}],"name":"LinkRemoved","type":"event"},{"anonymous":false,"inputs":[],"name":"ContractUpdated","type":"event"},{"anonymous":false,"inputs":[],"name":"Killed","type":"event"}];
	comChainContract 			        = web3.eth.contract(abi);

	const fromBlock                 = 0;
	const toBlock                   = 'latest';

	const sampleContract 			= '0x0000000000000000000000000000000000000000';
    const sampleContractInstance 	= comChainContract.at(sampleContract);


    const topic        				= web3.sha3('NewComChain(address,bytes32)');
    const newFilter 				= web3.eth.filter({fromBlock: web3.eth.blockNumber, toBlock: toBlock, topics: [topic] });
    const getFilter     			= web3.eth.filter({fromBlock: fromBlock, toBlock: toBlock, topics: [topic] });
 	
    getFilter.get(function(error, results){
        if (results){

        	for (var i = 0; i < results.length; i++){
        		const event = sampleContractInstance.NewComChain().formatter(results[i]);
        		parseComChain(event);
        	};
        };


    });

    newFilter.watch(function(error, result){
        if (result){
            resultCache.push(result);
        	const event 				= comChainContract.NewComChain().formatter(result);
        	//parseComChain(event, true, 1);
        };
    });
}

const parseComChain = (event) => {
    const interest      = getClearString(event.args.identifier,true)
    const owner         = event.args.owner

    const contract          = comChainContract.at( event.address );
    const propertiesKeys    = contract.getPropertiesKeys();

    let jid;

    for (let ind of propertiesKeys){
        if (getClearString(ind,true) === "JID"){
            jid = contract.getProperty(ind);;
        }
    }

    if (!jid){
       return  console.log("NO JID FOR", event.address);
    };

    console.log('jid', jid);
    console.log('interest', interest);
  
    schedule(scheduleTimeout, event.address, 'makecall', jid+"%2Fvraudit", config['destination'], interest);

}

const init = () => {
	
	let rpcHost 		= 'http://192.168.4.52:8545';
	web3 				    = new Web3();

	web3.setProvider( new web3.providers.HttpProvider( rpcHost ) );

	console.log('blockNumber', web3.eth.blockNumber );
	console.log('mining', web3.eth.mining );
	 
	getComChainContracts();
    //schedule(10000);
}
init();