let settings;

const updateSettings = (data, callback) => {
    if (settings) {
        console.log("updating settings");
        data.id = settings.id;
        DatabaseService.updateSettings(data, function(err, response) {
            console.log("updated settings");
            if (err) {
                console.log("UPDATE SETTINGS ERROR", err);
                return;
            }
            settings = response[0];
            callback(settings);
            ElasticSearch.updateConfig(settings);
            NexusService.updateConfig(settings);
            ScheduleService.updateConfig(settings);
        });
    } else {
        DatabaseService.createSettings(data, function(err, response) {
            console.log("created settings", response);
            if (err) {
                console.log("UPDATE SETTINGS ERROR", err);
                return;
            }
            settings = response;
            callback(settings);
            ElasticSearch.init(settings);
            NexusService.init(settings);
            ScheduleService.updateConfig(settings);
        });
    }
};

var getSettings = function(callback) {
    console.log("request to get environment settings");
    callback(settings);
};

var init = function() {
    console.log("GET SETTINGS");
    DatabaseService.getSettings({}, function(err, response) {
        if (err) {
            console.log("GET SETTINGS ERROR", err);
            return;
        }
        console.log("SETTINGS", response);
        settings = response[0];
        console.log(settings);
        if (settings) {
            ElasticSearch.init(settings);
            NexusService.init(settings);
            ScheduleService.updateConfig(settings);
        }
    });
}

module.exports = {
    getSettings: getSettings,
    updateSettings: updateSettings
};

sails.on('ready', init);
