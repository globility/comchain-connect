const Schedule            = require('node-schedule');
const request             = require('request');
const request_timeout     = 45 * 1000;

let config   			  = {
	'cas' 		: {
		'host'		: '',
		'port' 		: '',
		'https' 	: '',
	},
    'prefix'    : '/restapi/casapi',
	'actions' 	: {
        'getprofiles'  : '/dataservice/getprofiles/', // {jid}
        'getinterests' : '/dataservice/getinterests/', // {profile}
        'subscribe'    : '/pubsub/subscribe/', //{interest}/{jid}
        'makecall'     : '/callcontrol/makecall/', // {jid}/{destination}/{interest}
        'discoinfo'    : '/casapis/discovery/discoinfo',
        'discoitems'    : '/casapis/discovery/discoitems/' //{node}
	},
    'destination' : '',
    'length' : ''
};

var jobs = {};

var getJobs = function(callback) {
    console.log("GET JOBS");
    callback(jobs);
};

var removeJob = function(data, callback) {
    console.log("REMOVE JOB", data.id);
    DatabaseService.deleteData(data, function(err, response) {
        console.log("Removed database entry", err, response);
        if (jobs["job_" + data.id]) {
            var job = jobs["job_" + data.id];
            job.cancel();
            delete jobs["job_" + data.id];
            callback(jobs);
        } else {
            callback("No previous job to delete");
        }
    })
};

const schedule = (data, callback)  => {
    console.log("CREATE SCHEDULE", data);

    console.log('schedule time:', data.time);

    DatabaseService.createData({
        name: data.name,
        users: data.users,
        groups: data.groups,
        destination: data.destination,
        time: data.time,
        repeatFrequency: data.repeatFrequency,
        repeat: data.repeat,
        scheduled: data.scheduled,
        processed: false
    }, function(response) {
        console.log("Created database entry");
        data.id = response.id;
        jobs["job_" + response.id] = scheduleTask(data);
        jobs["job_" + response.id].data = data;
        //console.log("ALL JOBS:", jobs);
        console.log("NEW JOB CREATED " + data.id);
        callback(jobs);
    });
};

const updateSchedule = (data, callback) => {
    console.log("UPDATE SCHEDULE", data);

    console.log('schedule time:', data.time);

    DatabaseService.editData(data, function(err, response) {
        var response = response.length?response[0]:response;
        console.log("Updated database entry");
        console.log("Deleting job with id " + response.id)
        if (jobs["job_" + response.id]) {
            var job = jobs["job_" + response.id];
            job.cancel();
            delete jobs["job_" + response.id];
        } else {
            console.log("No previous job to delete");
        }
        console.log("Creating new job " + response.id);
        jobs["job_" + response.id] = scheduleTask(response);
        jobs["job_" + response.id].data = response;
        callback(jobs);
    });
};

const immediate = (data, callback) => {
    console.log("IMMEDIATE CALL", data);
    scheduleTask(data, callback);
    callback(data);
};

var mapUsers = function(users, groups) {
    var finalScheduleList = [];
    var userLineMap = {};

    var groupUsers = NexusService.getGroupUsers();

    for (var user in users) {
        userLineMap[user] = users[user];
    }

    for (var group in groups) {
        // skip loop if the property is from prototype
        if (!groups.hasOwnProperty(group)) continue;
        var groupUserList = groupUsers[group];
        for (var _i=0;_i<groupUserList.length;_i++) {
            var user = groupUserList[_i];
            userLineMap[user] = groups[group];
        };
    };

    _.each(_.keys(userLineMap), function(userId) {
        var userJid = NexusService.getUserFromId(userId).identifier;
        finalScheduleList.push({
            id: userId,
            identifier: userJid,
            lines: userLineMap[userId]
        });
    });

    console.log("FINAL SCHEUDLED ARRAY", finalScheduleList);
    return finalScheduleList;
};

var scheduleTask = function(data) {
    var data = data;
    console.log("SCHEDULE DATA", data);

    var stime;

    if (data.time) {
        dateTime = new Date(parseInt(data.time));
    } else {
        dateTime = new Date();
    };

    console.log("DATETIME", dateTime);

    var date = dateTime.getDate();
    var month = dateTime.getMonth() + 1;
    var day = dateTime.getDate();
    var hours = dateTime.getHours();
    var minutes = dateTime.getMinutes();
    var seconds = dateTime.getSeconds();

    seconds = seconds < 10? "0" + seconds:seconds;
    minutes = minutes < 10? "0" + minutes:minutes;
    hours = hours < 10? "0" + hours:hours;

    if (data.repeat) {
        if (data.repeatFrequency === "daily") { 
            var stime = seconds+' '+minutes+' '+hours+' * * 1-5';
        } else if (data.repeatFrequency === "weekends") {
            var stime = seconds+' '+minutes+' '+hours+' * * *';
        } else if (data.repeatFrequency === "immediate") {
            var stime = seconds+' '+minutes+' '+hours+' '+day+' '+month+' *';
        }
    } else {
        var stime = seconds+' '+minutes+' '+hours+' '+day+' '+month+' *';
    };

    console.log("STIME", stime);

    data.finalScheduleList = mapUsers(data.users, data.groups);

    console.log("FINAL SCHEUDLED ARRAY", data.finalScheduleList);

    //Schedule the task 
    if (data.repeatFrequency === "immediate") {
        var scheduleData = data;
        console.log("immediate test");
        for (var _i=0;_i<scheduleData.finalScheduleList.length;_i++) {
            var member = scheduleData.finalScheduleList[_i];
            //console.log("SCHEDULE MEMBER", member);
            verifyUserRegulated(scheduleData, member);
        }
    } else {
        var j = Schedule.scheduleJob( stime, function() {
            var scheduleData = data;
            if (scheduleData.pause === true) {
                console.log("Schedule is set to pause, will not run");
                return;
            } else {
                console.log("Schedule running");
                for (var _i=0;_i<scheduleData.finalScheduleList.length;_i++) {
                    var member = scheduleData.finalScheduleList[_i];
                    //console.log("SCHEDULE MEMBER", member);
                    verifyUserRegulated(scheduleData, member);
                }
            }
        });
        return j;
    }
};

var verifyUserRegulated = function(scheduleData, member) {
    var scheduleData = scheduleData;
    NexusService.getRegulated(member, function(response) {
        console.log("GET REGULATED STATUS CALLBACK " + member.identifier);
        var response = JSON.parse(response);
        console.log(response);
        console.log(response.regulated);
        if (response && response.regulated === true) {
            console.log("REGULATED " + member.identifier);
            verifyCASMapping(scheduleData, member);
        } else {
            console.log("NOT REGULATED " + member.identifier);
            var error = {
                message: "User " + member.identifier + " not regulated"
            }
            auditError(scheduleData, member, error);
        }
    });
};

var verifyCASMapping = function(scheduleData, member) {
    console.log('MAKE CALL contract address, jid, destination, interest ' + member.identifier);

    requestUrl("GET", "getprofiles", {jid: member.identifier}, function(api_response, body, error) {
        console.log("GET PROFILES RESPONSE " + member.identifier);
        if (error) {
            auditError(scheduleData, member, error);
            return;
        }
        if (!body) {
            var apiError = {
                message: api_response.statusMessage
            }
            auditError(scheduleData, member, apiError);
            return;
        }
        var profiles = JSON.parse(body);
        if (profiles.iq.command.note && profiles.iq.command.note.type === "error") {
            auditError(scheduleData, member, profiles.iq.command.note);
            return;
        }
        var profile = profiles.iq.command.iodata.out.profiles.profile;

        requestUrl("GET", "getinterests", {profile: profile.id}, function(api_response, body, error) {
            console.log("GET INTERESTS RESPONSE " + member.identifier);

            if (error) {
                auditError(scheduleData, member, error);
                return;
            }
            var interests = JSON.parse(body);
            if (interests.iq.command.note && interests.iq.command.note.type === "error") {
                auditError(scheduleData, member, interests.iq.command.note);
                return;
            }
            interests = interests.iq.command.iodata.out.interests.interest;
            if (!interests.length) {
                interests = [interests];
            }
            console.log(interests);

            // Check for active calls NOT in the busy state
            var interestCalls = _.pluck(interests, "callstatus");
            for (var _i=0;_i<interestCalls.length;_i++) {
                var call = interestCalls[_i];
                if (call) {
                    console.log("call found", call);
                    if (call.call && (call.call.state !== "CallBusy" || call.call.state !== "ConnectionCleared")) {
                        var error = {
                            message: "Active call found on device.",
                            type: "audit"
                        };
                        console.log("audit error", error);
                        auditError(scheduleData, member, error);
                        return;
                    }
                }
            }

            var scheduledLines = member.lines;
            console.log("TEST LINES", scheduledLines);
            var scheduledInterests = [];

            if (scheduledLines[0] === "default") {
                var interest = _.findWhere(interests, {default: true});
                if (!interest || interest === "undefined") {
                    var error = {
                        message: "No default interest found",
                        type: "audit"
                    };
                    console.log(error);
                    auditError(scheduleData, member, error);
                    return;
                }
                var contract = NexusService.getContractFromLine(interest.value);
                console.log("default interest and contract ", interest.id, contract.id);
                scheduledInterests.push({
                    line: "default",
                    profile: profile.id,
                    interest: interest.id,
                    contract: contract.id
                });
            } else if (scheduledLines[0] === "all") {
                console.log("not yet implemented");
                return;
            } else {
                for (var _i=0;_i<scheduledLines.length;_i++) {
                    var line = scheduledLines[_i];
                    var interest = _.findWhere(interests, {value: parseInt(line)});
                    var contract = NexusService.getContractFromLine(line);
                    if (!interest || interest === "undefined") {
                        console.log("unable to find interest matching line", line, interests);
                        var error = {
                            message: "No interest matching user's line " + line,
                            type: "audit"
                        };
                        auditError(scheduleData, member, error, '', line);
                        return;
                    } else {
                        console.log("line and interest and contract", line, interest.id, contract.id);
                        scheduledInterests.push({
                            line: line,
                            profile: profile.id,
                            interest: interest.id,
                            contract: contract.id
                        });
                    }
                }
            }
            console.log("SCHEDULED", scheduledInterests);

            makeCall(scheduleData, scheduledInterests, member);  
        });
    });
};

var makeCall = function(scheduleData, scheduledInterests, member) {
    var scheduleData = scheduleData;
    var scheduledInterests = scheduledInterests;
    var member = member;

    //console.log("issuing make-calls for " + member.identifier + ":", scheduledInterests);
    var interest = scheduledInterests[0];

    if (scheduleData.scheduled) {
        var destination = config.destination;
    } else {
        var destination = scheduleData.destination;
    }

    var subscribeCallback = function(api_response, body, error) {
        console.log("SUBSCRIBE RESPONSE " + member.identifier);

        if (error) {
            auditError(scheduleData, member, error);
            return;
        }

        var response = JSON.parse(body);
        if (response.iq && response.iq.type === "error") {
            console.log("SUBSCRIBE FAILED");
            var subscribeError = {};
            for (let key in response.iq.error) {
                if (key !== "type" && key !== "code") {
                    subscribeError.message = key;
                }
            }
            console.log(subscribeError);
            auditError(scheduleData, member, subscribeError);
        } else if (response.iq && response.iq.type === "result") {
            console.log("SUBSCRIBED " + member.identifier);
            if (response.iq.command && response.iq.command.note && response.iq.command.note.type === "error") {
                console.log("subscribe error: ", response.iq.command.note.content);
                var subscribeError = {};
                subscribeError.message = response.iq.command.note.content;
                auditError(scheduleData, member, subscribeError);
                return;
            }
            var makeCallCallback = function(api_response, body, error) {
                console.log("MAKE-CALL CALLBACK" + member.identifier);
                if (error) {
                    auditError(scheduleData, member, error);
                    return;
                }
                if (body) {
                    try {
                        var response = JSON.parse(body);
                    } catch (e) {
                        var response = body;
                    }
                } else {
                    var makeCallError = {};
                    if (api_response && api_response.statusMessage) {
                        makeCallError.message = "Make Call " + api_response.statusMessage;
                        auditError(scheduleData, member, makeCallError);
                        return;
                    } else {
                        makeCallError.message = "Unknown failure in Make-Call Request";
                        auditError(scheduleData, member, makeCallError);
                        return;
                    }
                }
                
                makeCallResponse(scheduleData, scheduledInterests, member, response);
            };
            
            requestUrl("POST", "makecall", {
                jid: member.identifier,
                destination: destination,
                interest: interest.interest
            }, makeCallCallback);
        }
    }
    requestUrl("GET", "subscribe", {
        jid: member.identifier,
        interest: interest.interest
    }, subscribeCallback);
}

var makeCallResponse = function(scheduleData, scheduledInterests, member, response) {
    console.log("MAKE CALL RESPONSE " + member.identifier, response);

    console.log(scheduledInterests);

    var line = scheduledInterests[0].line;
    var interest = scheduledInterests[0].interest;
    var contract = scheduledInterests[0].contract;

    if (!contract || contract === "error") {
        console.log("FAILED TO FIND A VALID CONTRACT");
        return;
    }

    var auditJson = {
        type              : 'audit',
        comid            : '',
        state             : '',
        cscid             : '',
        interestid        : '',
        additionalProperty: [],
        starttimestamp    : new Date().toUTCString()
    };

    if (response.iq && response.iq.error) {
        console.log("makecall error");

        for (let key in response.iq.error) {
            if (key !== "type" && key !== "code") {
                var errorMessage = key;
            }
        };
        auditJson.state = "audit-failure";
        auditJson.interestid = interest;
        auditJson.additionalProperty.push({
            name: "error",
            value: errorMessage
        });
        auditJson.additionalProperty.push({
            name: "scheduleId",
            value: scheduleData.id
        });
        auditJson.additionalProperty.push({
            name: "scheduleName",
            value: scheduleData.name
        });
        auditJson.additionalProperty.push({
            name: "jid",
            value: member.identifier
        });
    };

    if (response.iq && response.iq.command && !response.iq.error) {
        if (response.iq.command.note && response.iq.command.note.type === "error") {
            console.log("makecall error: ", response.iq.command.note.content);
            var errorMessage = response.iq.command.note.content;
            auditJson.state = "audit-failure";
            auditJson.interestid = interest;
            auditJson.additionalProperty.push({
                name: "error",
                value: errorMessage
            });
            auditJson.additionalProperty.push({
                name: "scheduleId",
                value: scheduleData.id
            });
            auditJson.additionalProperty.push({
                name: "scheduleName",
                value: scheduleData.name
            });
            auditJson.additionalProperty.push({
                name: "jid",
                value: member.identifier
            });
        } else {
            var call = response.iq.command.iodata.out.callstatus.call;
            console.log("MAKE CALL RESPONSE SUCCESSFUL");
            auditJson.comid = call.id;
            auditJson.state = "audit-success";
            auditJson.interestid = call.interest;
            auditJson.additionalProperty.push({
                name: "scheduleId",
                value: scheduleData.id
            });
            auditJson.additionalProperty.push({
                name: "scheduleName",
                value: scheduleData.name
            });
            auditJson.additionalProperty.push({
                name: "jid",
                value: member.identifier
            });
            var origRef = call["originator-ref"].property;
            for (var _i=0;_i<origRef.length;_i++) {
                if (origRef[_i].id === "gcid") {
                    auditJson.cscid = origRef[_i].value;
                }
            }
        };
    };

    console.log(auditJson);

    NexusService.updateContract(contract, auditJson);

    console.log("before deleting the interest", scheduledInterests);
    scheduledInterests.shift();
    console.log("after deleting the interest", scheduledInterests);
    if (scheduledInterests.length > 0) {
        continueMakeCall(scheduleData, scheduledInterests, member);
    }

    if (!scheduleData.repeat && scheduleData.repeatFrequency !== "immediate") {
        completeJob(scheduleData);
    }                
};

var auditError = function(scheduleData, member, error, scheduledInterests, line) {
    var auditJson = {
        type              : 'audit',
        comid            : '',
        state             : '',
        cscid             : '',
        interestid        : '',
        additionalProperty: [],
        starttimestamp    : new Date().toUTCString()
    };
    var errorObj = {
        name: "error",
        value: ""
    };
    console.log("audit error", error);

    if (error.code) {
        //console.log("err code", error.code);
        errorObj.value = error.code;
        auditJson.additionalProperty.push(errorObj)
        auditJson.state = "connection-failure";
    }
    if (error.content) {
        //console.log("err content", error.content);
        errorObj.value = error.content;
        auditJson.additionalProperty.push(errorObj)
        auditJson.state = "audit-failure"
    }
    if (error.warning) {
        //console.log("err warning", error.warning);
        errorObj.value = error.warning;
        auditJson.additionalProperty.push(errorObj)
        auditJson.state = "audit-warning"
    }
    if (error.message) {
        //console.log("err message", error.message);
        errorObj.value = error.message;
        auditJson.additionalProperty.push(errorObj)
        auditJson.state = "audit-failure"
    }
    auditJson.additionalProperty.push({
        name: "scheduleId",
        value: scheduleData.id
    });
    auditJson.additionalProperty.push({
        name: "scheduleName",
        value: scheduleData.name
    });
    auditJson.additionalProperty.push({
        name: "jid",
        value: member.identifier
    });
    console.log(auditJson);
    console.log(auditJson.additionalProperty);

    var memberObj = NexusService.getUserFromId(member.id);
    var memberContracts = memberObj.contracts;

    if (scheduledInterests) {
        for (var _i=0;_i<scheduledInterests.length;_i++) {
            var contract = scheduledInterests[_i].contract;
            auditJson.interestid = scheduledInterests[_i].interest;
            auditJson.profileid = scheduledInterests[_i].profile;
            NexusService.updateContract(contract, auditJson);
        }
    } else if (line) {
        var contract = _.findWhere(memberContracts, {line: line});
        auditJson.interestid = line;
        NexusService.updateContract(contract.id, auditJson);
    } else if (member.lines[0] === "all") {
        for (var _i=0;_i<memberContracts.length;_i++) {
            var contract = memberContracts[_i];
            auditJson.interestid = contract.identifier;
            NexusService.updateContract(contract.id, auditJson);
        }
    } else if (member.lines[0] === "default") {
        if (memberContracts.length > 0) {
            var contract = memberContracts[0];
            auditJson.interestid = contract.identifier;
            NexusService.updateContract(contract.id, auditJson);
        }
    } else {
        for (var _i=0;_i<member.lines.length;_i++) {
            var line = member.lines[_i];
            var contract = _.findWhere(memberContracts, {line: line});
            auditJson.interestid = line;
            NexusService.updateContract(contract.id, auditJson);
        }
    }

    if (!scheduleData.repeat) {
        completeJob(scheduleData);
    }  
};

var completeJob = function(data) {
    DatabaseService.completeData({
        id: data.id
    }, function(response) {
        console.log("COMPLETE database entry");
        if (jobs["job_" + data.id]) {
            console.log("Deleting Job " + data.id);
            var job = jobs["job_" + data.id];
            job.cancel();
            delete jobs["job_" + data.id];
            console.log("completeJob callback", jobs);
        }  
    });
};

var continueMakeCall = function(scheduleData, scheduledInterests, member) {
    // do a get-interests then issue makeCall();
    console.log("Need to continue make calls for: ", scheduledInterests);
    var schedule = scheduledInterests[0];
    
    requestUrl("GET", "getinterests", {profile: schedule.profile}, function(api_response, body, error) {
        console.log("GET INTERESTS RESPONSE " + member.identifier);
        if (error) {
            auditError(scheduleData, member, error);
            return;
        }
        var interests = JSON.parse(body);
        if (interests.iq.command.note && interests.iq.command.note.type === "error") {
            auditError(scheduleData, member, interests.iq.command.note);
            return;
        }
        interests = interests.iq.command.iodata.out.interests.interest;
        if (!interests.length) {
            interests = [interests];
        }
        console.log(interests);

        // Check for active calls NOT in the busy state
        var interestCalls = _.pluck(interests, "callstatus");
        for (var _i=0;_i<interestCalls.length;_i++) {
            var call = interestCalls[_i];
            if (call) {
                console.log("call found", call);
                if (call.call && (call.call.state !== "CallBusy" || call.call.state !== "ConnectionCleared")) {
                    // check if it's an audit call
                    if (call.call["originator-ref"] && call.call["originator-ref"].property) {
                        console.log("originator ref found", call.call["originator-ref"]);
                        var origRef = call.call["originator-ref"].property;
                        for (var _i=0;_i<origRef.length;_i++) {
                            console.log("orig ref", origRef[_i]);
                            if (origRef[_i].id === "call-type" && origRef[_i].value === "auditcall") {
                                console.log("this is an audit call, wait another " + config["length"] + " seconds");
                                setTimeout(function() {
                                    continueMakeCall(scheduleData, scheduledInterests, member)
                                }, (config['length'] * 1000) + 10000)
                                return;
                            }
                        }
                    }
                    // not an audit call? report error
                    var error = {
                        message: "Active call found on device.",
                        type: "audit"
                    };
                    console.log("audit error", error);
                    auditError(scheduleData, member, error, scheduledInterests);
                    return;
                }
            }
        }

        console.log("REPEAT SCHEDULED", scheduledInterests);

        makeCall(scheduleData, scheduledInterests, member);                 
    });
};

var validateSchedule = function(scheduleData, callback) {
    var data = scheduleData;

    var userRegulatedMap = {
        users: [],
        groups: []
    };

    var nexusUsers = NexusService.getUsers();
    var nexusGroups = NexusService.getGroups();

    var scheduleUsers = [];
    var scheduleGroups = [];

    for (let user in data.users) {
        scheduleUsers.push(user);
    };
    for (let group in data.groups) {
        scheduleGroups.push(group);
    };

    validateCisco();
    
    if (scheduleUsers.length > 0) {
        console.log("validating users");
        validateUsers(scheduleUsers, userRegulatedMap, function(map) {
            console.log("UPDATED USER MAP", map);
            userRegulatedMap = map;
            if (scheduleGroups.length > 0) {
                console.log("validating groups");
                validateGroups(scheduleGroups, userRegulatedMap, function(map) {
                    console.log("UPDATED GROUP MAP", map);
                    userRegulatedMap = map;
                    callback(userRegulatedMap)
                })
            } else {
                console.log("no groups to validate");
                callback(userRegulatedMap);
            }
        })
    } else if (scheduleGroups.length > 0) {
        console.log("no users to validate, validating groups");
        validateGroups(scheduleGroups, userRegulatedMap, function(map) {
            console.log("UPDATED GROUP MAP", map);
            userRegulatedMap = map;
            callback(userRegulatedMap)
        })
    } else {
        console.log("no users or groups to validate");
        callback(userRegulatedMap);
    }
};

var validateUsers = function(users, map, callback) {
    for (var _i=0;_i<users.length;_i++) {
        var user = users[_i];
        NexusService.getRegulated(user, function(response, member) {
            console.log("member regulated callback");
            users.shift();
            if (response) {
                map.users.push({
                    user: member,
                    regulated: response.regulated = true?true:false
                })
            }
            //console.log("Current users", users);
            if (users.length < 1) {
                console.log("User Regulation Done");
                callback(map);
            }
        })
    };
};

var validateGroups = function(groups, map, callback) {
    for (var _j=0;_j<groups.length;_j++) {
        var group = groups[_j];
        NexusService.getRegulatedGroup(group, function(response, group) {
            console.log("group regulated callback");
            groups.shift();
            if (response) {
                map.groups.push({
                    group: group,
                    regulated: response.regulated = true?true:false
                })
            }
            //console.log("Current groups ", groups);
            if (groups.length < 1) {
                console.log("Group Regulation Done");
                callback(map);
            }
        })
    };
};

var validateCisco = function() {
    requestUrl("GET", "discoitems", {
        node: encodeURIComponent('http://xmpp.org/protocol/openlink#vr-profiles')
    }, function(response, body, error) {
        console.log("DISCO ITEMS RESPONSE", body);
    });
};

var init = function() {
    DatabaseService.findData({
        or: [
            { processed: false },
            { repeat: true }
        ]
    }, function(response) {
        var tasksToSchedule = response;
        console.log("Startup - Scheduled DB Jobs");
        tasksToSchedule.forEach(function(task, index) {
            jobs["job_" + task.id] = scheduleTask(task);
            if (jobs["job_" + task.id]) {
                jobs["job_" + task.id].data = task;
            }
        });
    });
};

var requestUrl = function(type, action, metadata, callback, errCallback) {
    let auth = "Basic " + new Buffer('admin' + ":" + 'Pa55w0rd').toString("base64");
    let url = 'http://';
    if (config['cas'].https === true)
        url = 'https://';

    url += config['cas'].host + ":" + config['cas'].port + config.prefix;

    if (metadata && metadata.interest) {
        metadata.interest = encodeURIComponent(metadata.interest);
    }

    if (metadata && metadata.destination) {
        metadata.destination = encodeURIComponent(metadata.destination);
    }

    if (action) {
        url += config.actions[action];
        if (action === "subscribe") {
            url += metadata.interest + '/' + metadata.jid;
        } else if (action === "makecall") {
            url += metadata.jid + '/' + metadata.destination + '?interest=' + metadata.interest;
        } else if (action === "getprofiles") {
            url += metadata.jid;
        } else if (action === "getinterests") {
            url += metadata.profile;
        } else if (action === "discoitems") {
            url += metadata.node;
        }
    }

    console.log(action + " - " + url);

    var options = {
        url: url,
        method: type,
        timeout: request_timeout,
        rejectUnauthorized: false,
        headers : {
            "Authorization" : auth
        }
    };

    if (action == "makecall") {
        options.json = [{
            id: 'call-type',
            value: 'auditcall'
        }];
    }

    var requestCallback = function(error, api_response, body) {
        if (error) {
            console.log("Request URL Error");
            return callback(api_response, body, error);
        }
        callback(api_response, body);
    }
    request(options, requestCallback);
};

var updateConfig = function(settings) {
    console.log("Updating Scheduler Settings");
    config['cas'].host = settings.casHost;
    config['cas'].port = settings.casPort;
    config['cas'].https = settings.casHttps;
    config.destination = settings.destination;
    config.length = settings.messageLength;
};

module.exports = {
    init: init,
    updateConfig: updateConfig,
	schedule: schedule,
    updateSchedule: updateSchedule,
    immediate: immediate,
    validate: validateSchedule,
    // reschedule: rescheduleTask,
    getJobs: getJobs,
    removeJob: removeJob
};
