'use strict'

const jsonfile 			= require('jsonfile');
const request			= require('request');

const updateContract = (address, data) => {
	const fileName 	= 'at_'+address+"_"+new Date(data.vraudittimestamp).getTime();
	let file 		= '/opt/nfs/comchain/'+fileName+'.json'

	console.log('file',file);
	 
	jsonfile.writeFile(file, data, function (err) {
	  	console.error(err)

	  	if (err)
	  		return console.log(err);

	  	web3.personal.unlockAccount(admin.address, admin.password, 80, (error, result) => {
	  		console.log("UNLOCK",error, result);
	     	if (error){
	     		sails.log.error(error);
	     	} else {
	     		const contract  = comChainContract.at( address );
				const hash 		= web3.sha3(JSON.stringify(data));

				contract.pushMetaData( hash, file,
		            { from: admin.address, gas: 276010 },
		            (err, resp) => {
		                sails.log('pushMetaData RESP:');
		                sails.log('err', err);
		                sails.log('resp', resp);
		            });
	     	};
    	});
		
	})
};

module.exports = {
	updateContract: updateContract
};


// sails.on('ready', init);

