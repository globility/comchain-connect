const Schedule            = require('node-schedule');
const request             = require('request');
const jsonfile            = require('jsonfile');
const sha256              = require('sha256');
const request_timeout     = 45 * 1000;
const _                   = require('underscore')._;

let config   			  = {
	'nexus' 		: {
		'host'		: '',
		'port' 		: '',
		'https' 	: '',
        'token'     : ''
	},
    'prefix'    : '/v1',
    'actions'   : {
        'contracts' : '/product',
        'template'  : '/comchain/_id',
        'community' : '/community',
        'members'   : '/community/_id/member',
        'groups'    : '/community/_id/group?regulated=true',
        'metadata'  : '/product/_id/metadata',
        'regulated' : '/member/_id/regulated',
        'regulated-group' : '/community/_id-comm/group/_id/regulated'
    }
};

let communityArray = [];
let communityConcat = {};
let membersArray = [];
let nonRegulatedMembersArray = [];
let contractsArray = [];
let groupsArray = [];
let groupsObject = {};
let nonRegulatedMembersObject = {};
let memberProductConcat = {};
let groupUserArray = {};
let contractsRequest;

var getCommunities = function(callback) {
    console.log("GET COMMUNITIES");
    if (callback) {
        callback(communityArray);
    } else {
        return communityArray;
    }   
};

var getCommunityConcat = function(callback ){
    console.log("GET COMMUNITY CONCAT");
    if (callback) {
        callback(communityConcat);
    } else {
        return communityConcat;
    }   
}

var getUsers = function(callback) {
    console.log("GET USERS");
    if (callback) {
        callback(memberProductConcat);
    } else {
        return memberProductConcat;
    }   
};

var getMembers = function(callback) {
    console.log("GET MEMBERS");
    if (callback) {
        callback(membersArray);
    } else {
        return membersArray;
    }   
};

var getUnregulatedUsers = function(callback) {
    console.log("GET UNREGULATED USERS");
    if (callback) {
        callback(nonRegulatedMembersObject);
    } else {
        return nonRegulatedMembersObject;
    }   
};

var getUserFromId = function(userId) {
    var response = "error";
    var user = memberProductConcat[userId];
    if (user) {
        response = user;
    }
    return response;
};

var getUserFromJid = function(jid) {
    var response = "error";
    var user = _.findWhere(membersArray, {identifier: jid});
    if (user) {
        response = user;
    }
    return response;
};

var getGroups = function(callback) {
    console.log("GET GROUPS");
    if (callback) {
        callback(groupsArray);
    } else {
        return groupsArray;
    }   
};

var getGroupUsers = function(callback) {
    console.log("GET GROUP USERS");
    if (callback) {
        callback(groupUserArray);
    } else {
        return groupUserArray;
    }
}

var getContracts = function(callback) {
    console.log("GET CONTRACTS");
    if (callback) {
        callback(contractsArray);
    } else {
        return contractsArray;
    }   
};

var getContract = function(member, callback) {
    var jid = member.identifier;
    console.log("GET CONTRACT FOR " + jid);
    var response = "error";
    for (var _i=0;_i<contractsArray.length;_i++) {
        var contract = contractsArray[_i];
        var properties  = contract.additionalProperty;
        for (var _j=0;_j<properties.length;_j++) {
            if (properties[_j].name === "user" && properties[_j].value === jid) {
                response = contract;
            }
        }
    }
    return callback(response);   
};

var getContractTemplate = function(comchain, callback) {
    var id = comchain.id;
    var action = "template";
    requestUrl("GET", action, id, null, function(response, body) {
        return callback(body);
    }, function(error) {
        console.log(error);
        return callback(error);
    });
};

var getNameFromUser = function(userId) {
    //console.log("GET Name FOR " + userId);
    userId = userId.split('@')[0];
    var response = "error";
    var memberId = _.find(membersArray, {identifier: userId});
    if (memberId) {
        response = memberId.name;
    }
    return response;   
};

var getContractFromCallId = function(line, device) {
    //console.log("GET CONTRACT FOR " + line + " " + device);
    var response = "error";
    for (var _i=0;_i<contractsArray.length;_i++) {
        var contract = contractsArray[_i];
        if (contract.identifier.indexOf(line) > -1
            && contract.identifier.indexOf(device > -1)) {
            var userId = getUserFromId(contract.owner['@id']);
            response = userId;
            return response;
        }
    }
};

var getContractFromLine = function(line) {
    console.log("GET CONTRACT FOR " + line);
    line = line.toString();
    var response = "error";
    var contract = _.findWhere(contractsArray, {identifier: line});
    if (contract) {
        response = contract;
    }
    return response;
};

var getContractMetadata = function(address) {
    address = "0xa5ef3f0a83ca28addf51126b26ce4c40280caaea";
    var action = "metadata";
    requestUrl("GET", action, address, null, function(api_response, body) {
        console.log("Successfully retrieved metadata for contract " + address, body);
    }, function(err) {
        console.log("Failed to retrieve metadata for contract " + address, err)
    });
};

var getRegulated = function(member, callback) {
    console.log("MEMBER REGULATED STATUS");
    var action = "regulated";
    var memberId = member.id? member.id:member;
    requestUrl("GET", action, memberId, null, function(response, body) {
        //console.log("Member regulated");
        return callback(body, memberProductConcat[memberId]);
    }, function(error) {
        //console.log("Error getting member regulatory status", error);
        return callback(error, memberProductConcat[memberId]);
    });
};

var getRegulatedGroup = function(group, callback) {
    console.log("GROUP REGULATED STATUS");
    var action = "regulated-group";
    var groupId = group.id? group.id:group;
    requestUrl("GET", action, groupId, null, function(response, body) {
        //console.log("Group regulated");
        return callback(body, groupsObject[groupId]);
    }, function(error) {
        //console.log("Error getting group regulatory status", error);
        return callback(error, groupsObject[groupId]);
    });
};

var pollCommunities = function() {
    console.log("POLL COMMUNITY");
    var action = "community";
    requestUrl("GET", action, null, null, function(response, body) {
        var communities = JSON.parse(body);
        communityArray = communities;
        //console.log("COMMUNITIES RESPONSE", communities);
        for (var _i=0;_i<communities.length;_i++) {
            var community = communities[_i];
            communityConcat[community.id] = {
                members: [],
                groups: []
            };
            pollGroups(community);
        };
    }, function(error) {
        console.log("Error polling Communities", error);
    });
};

var pollGroups = function(community) {
    var community = community;
    var action = "groups";
    requestUrl("GET", action, community.id, null, function(response, body) {
        console.log("Retrieved Groups from " + community.id);
        var groups = JSON.parse(body);
        for (var _i=0;_i<groups.length;_i++) {
            var group = groups[_i];
            if (group && group.regulated) {
                group.community = community.id;
                if (communityConcat[community.id]) {
                    communityConcat[community.id].groups.push(group.id);
                }
                groupsArray.push(group);
                groupUserArray[group.id] = [];
                groupsObject[group.id] = group;
            }
        }
        //console.log("GROUP USER ARRAY", groupUserArray);
        //console.log("GROUPS ARRAY", groupsArray);
        pollMembers(community);
    }, function(error) {
        console.log("Error polling groups", error);
    })
};

var pollMembers = function(community) {
    var community = community;
    console.log("POLL MEMBERS");
    action = "members";
    requestUrl("GET", action, community.id, null, function(response, body) {
        console.log("Retrieved Members from " + community.id);
        var members = JSON.parse(body);
        for (var _i=0;_i<members.length;_i++) {
            var member = members[_i];
            if (member.regulated === true) {
                member.community = community.id;
                if (communityConcat[community.id]) {
                    communityConcat[community.id].members.push(member.id);
                }
                membersArray.push(member);
                member.contracts = [];
                memberProductConcat[member.id] = member;
                var groups = member.groups;
                for (group in groups) {
                    var groupAddress = groups[group];
                    if (groupUserArray[groupAddress]) {
                        groupUserArray[groupAddress].push(member.id);
                    }
                }
            } else {
                nonRegulatedMembersArray.push(member);
                nonRegulatedMembersObject[member.id] = member;
            }
        }
        console.log("MEMBERS ARRAY", membersArray);
        //console.log("NON REG MEMBERS ARRAY", nonRegulatedMembersArray);
        //console.log("GROUP USER ARRAY", groupUserArray);
        console.log("COMMUNITY CONCAT ARRAY", communityConcat);

        if (!contractsRequest || contractsRequest === false) {
            pollContracts();
            contractsRequest = true;
        }
    }, function(error) {
        console.log("Error polling Members", error);
    })
};

var pollContracts = function() {
    console.log("POLL CONTRACTS");
    var action = "contracts";
    requestUrl("GET", action, null, null, function(response, body) {
        console.log("Retrieved Contracts");
        contractsRequest = false;
        contractsArray = [];
        contracts = JSON.parse(body);
        for (var _i=0;_i<contracts.length;_i++) {
            var contract = contracts[_i];
            contract.owners = [];
            for (var _j=0;_j<contract.owner.length;_j++) {
                var owner = contract.owner[_j];
                contract.owners.push(owner['@id']);
                var member = memberProductConcat[owner['@id']];
                if (member && !_.contains(member.contracts, contract.id)) {
                    member.contracts.push({
                        id: contract.id,
                        line: contract.identifier
                    });
                }
            }
            contractsArray.push(contract);
        }
        //console.log("CONTRACTS ARRAY", contractsArray);
        //console.log("USERS CONCAT ARRAY", memberProductConcat);
        ScheduleService.init();
    }, function(error) {
        console.log("Error polling Contracts", error);
    });
};

var updateContract = function(address, data) {
    requestUrl("PUT", "metadata", address, data, function(response, body) {
        //Success Callback
        console.log("Successfully updated Contract Metadata", body);
    }, function(error) {
        //Error Callback
        console.log("Error updating Contract Metadata", error);
    });
};

var requestUrl = function(type, action, urldata, metadata, callback, errCallback) {
    //let auth = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZGRyZXNzIjoiMHg1MDM4ZjI3YTJjZDQ1ZDY2ZjIzNDgyNGRkY2E3OGM2OTVjM2Y4ZGYwIiwicGFzc3BocmFzZSI6ImYyOWJmNzQ4NmZjYTg0NWZhOWQ0YmQzMDcwNDc5M2RlMTI3ZjI5MzdjZDQxOTEzNjZlYzlmNmM1ZmQxMjBkYTM0M2UwNGZiNWY1ZjgwODU0ZmIyOGUwNTg0NTk3YzIyMHIyei84UlUwT3JVUU52eW9NUWkvT0E9PSIsImlhdCI6MTUyMjkyODQ3NiwiZXhwIjoxNTU0NDg2MDc2fQ.bPR6nheij9sysibzZTA2xjb9eajPBiaJdXrhJjkaPNk";
    if (!config['nexus'].token) {
        console.log("No Nexus Token defined");
        return;
    }
    let auth = "Bearer " + config['nexus'].token;
    let url = 'http://';
    if (config['nexus'].https === true)
        url = 'https://';

    url += config['nexus'].host + ":" + config['nexus'].port + config.prefix;

    var actionUrl = config.actions[action];
    if (actionUrl.indexOf("_id-comm") > -1) {
        actionUrl = actionUrl.replace("_id-comm", communityArray[0].id);
    }
    if (actionUrl.indexOf("_id") > -1) {
        actionUrl = actionUrl.replace("_id", urldata);
    }
    url += actionUrl;

    console.log(url);

    var options = {
        url: url,
        method: type,
        timeout: request_timeout,
        rejectUnauthorized: false,
        headers : {
            "Authorization" : auth
        }
    };
    if (metadata) {
        options.json = metadata;
    }

    var requestCallback = function(error, api_response, body) {
        if (error) {
            console.log("Request URL Error");
            console.log(error, api_response, body);
            return errCallback(error);
        }
        callback(api_response, body);
    }
    request(options, requestCallback);
};

var updateConfig = function(settings) {
    console.log("Updating Nexus Config");
    config['nexus'].host = settings.nexusHost;
    config['nexus'].port = settings.nexusPort;
    config['nexus'].https = settings.nexusHttps;
    config['nexus'].token = settings.nexusToken;
}

var init = function(settings) {
    updateConfig(settings);
    pollCommunities();
};

module.exports = {
    init: init,
    updateConfig: updateConfig,
    getCommunities: getCommunities,
    getCommunityConcat: getCommunityConcat,
    getUsers: getUsers,
    getMembers: getMembers,
    getUnregulatedUsers: getUnregulatedUsers,
    getUserFromId: getUserFromId,
    getUserFromJid: getUserFromJid,
    getNameFromUser: getNameFromUser,
    getGroupUsers: getGroupUsers,
    getGroups: getGroups,
	getContracts: getContracts,
    getContract: getContract,
    getContractTemplate: getContractTemplate,
    getContractFromCallId: getContractFromCallId,
    getContractFromLine: getContractFromLine,
    getRegulated: getRegulated,
    getRegulatedGroup: getRegulatedGroup,
    updateContract: updateContract
};

//sails.on('ready', init);
