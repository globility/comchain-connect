const elasticsearch = require('elasticsearch');
let client;


const username 	= "elastic";
const password 	= "changeme";

let host 		= "192.168.3.127"//"ab-tools";
let port 		= "9200";

const indexPrefix = 'ccc';
const globalType = 'data';

const indices 	= {
	'link' 		: 'link',
	'linkdev' 	: 'linkdev',
	'node' 		: 'node',
	'mde' 		: 'mderecord-new'
};


const getIndexName = ( n ) => {
	return indexPrefix + '-' + indices[n];
};

var updateConfig = function(settings) {
	console.log("Updating ES Config");
    host = settings.dataSourceHost;
    port = settings.dataSourcePort;
}

var init = function(settings) {
	updateConfig(settings);
	connect();
};

const getRecords = (emmit, range) => new Promise(function(resolve, reject) {
	const cb = (error,response) => {
		if (response && response.hits && response.hits.hits.length > 0){

			let items 	= _.pluck(response.hits.hits, '_source');

			items = _.sortBy(items, function(item) {
				var starttimestampField = _.findWhere(item.metadata, {name: 'starttimestamp'});
				return new Date(starttimestampField['starttimestamp']).getTime();
			}).reverse();

			let types =   _.uniq(_.flatten(_.map(items, 'types')));
			//console.log("TYPES", types)
			let providers = _.flatten(_.map(items, 'providers'));
			//console.log("PROVIDERS", providers);

			var data = {
				telephony: {
					sources: {},
					sourcesHashed: {},
					sourcesCount: 0,
					//records: [],
					recordsCount: 0,
					recordsCountHashed: 0
				},
				voicerecorder: {
					sources: {},
					sourcesHashed: {},
					sourcesCount: 0,
					//records: [],
					recordsCount: 0,
					recordsCountHashed: 0
				},
				audit: {
					sources: {},
					sourcesHashed: {},
					sourcesCount: 0,
					//records: [],
					recordsCount: 0,
					recordsCountHashed: 0
				},
				sms: {
					sources: {},
					sourcesHashed: {},
					sourcesCount: 0,
					//records: [],
					recordsCount: 0,
					recordsCountHashed: 0
				},
				records: [],
				dates: []
			};

			let unregulatedUsers = NexusService.getUnregulatedUsers();
			//console.log("UNREG USERS", unregulatedUsers);

			for (let type of types) {
				var typeProviders = _.filter(providers, {type: type});
				var typeProvidersMap = _.flatten(_.map(typeProviders, 'provider'));
				//console.log("TYPE : " + type, typeProvidersMap);
				if (data[type]) {
					data[type].sourcesCount = _.uniq(typeProvidersMap).length;
					for (let provider of typeProvidersMap) {
						//console.log("PROV ID", provider)
						var providerObj = _.findWhere(unregulatedUsers, {id: provider});
						if (providerObj) {
							var providerName = providerObj.identifier;
						}
						//console.log("PROV STUFF", providerObj, providerName);
						if (providerName && data[type].sources[providerName]) {
							data[type].sources[providerName]++;
						} else if (providerName) {
							data[type].sources[providerName] = 1;
						}
					}
				}
				//console.log("UPDATED DATA TYPE", data[type]);
			}

			for (let item of items) {
				for (let type of item.types) {
					//data[type].records.push(item);
					data[type].recordsCount++;
					if (item.isValid) {
						data[type].recordsCountHashed++;
					}
					for (let provider of item.providers) {
						var providerObj = _.findWhere(unregulatedUsers, {id: provider.provider});
						if (providerObj) {
							var providerName = providerObj.identifier;
						}
						if (providerName && item.isValid && data[type].sourcesHashed[providerName]) {
							data[type].sourcesHashed[providerName]++;
						} else if (providerName && item.isValid) {
							data[type].sourcesHashed[providerName] = 1;
						}
					}
				}
				var metadata = item.metadata;
				var additionalProperty = _.findWhere(metadata, {name: "additionalProperty"}).additionalProperty;
				var jid = _.findWhere(additionalProperty, {name: "jid"}).value;
				var userObj = NexusService.getUserFromJid(jid);
				item.owner = userObj;
				data.records.push(item);
				var date = _.findWhere(item.metadata, {name: 'starttimestamp'});
				date = new Date(date.starttimestamp);
				date = date.getUTCDate() + "/" + (date.getUTCMonth() + 1) + "/" + date.getFullYear();
				data.dates.push(date);
			}
			data.date = _.uniq(data.dates);

			var output = {
				data		: data
			};
			resolve(output);
		} else {
			console.log("Empty response?");
		}
	};
	range = range.range;
	console.log(range);
	if (range.startDate) {
		range.startDate = range.startDate.split('T')[0];
	}
	if (range.endDate) {
		range.endDate = range.endDate.split('T')[0];
	}
	console.log(range);
	var data = {
		"from": 0,
		"size": 10000,
	    "query": {
	        "range" : {
	            "metadata.starttimestamp" : {
	                "gte": range.startDate? range.startDate:'0000',
	                "lte": range.endDate? range.endDate:'9999',
	                "format": "yyyy-MM-dd||yyyy"
	            }
	        }
	    }
	}
	search( getIndexName('mde'), globalType, data, cb)
});

const connect = () =>{
	if (process.env.ES_USERNAME)
		username 	= process.env.ES_USERNAME;

	if (process.env.ES_PASSWORD)
		password 	= process.env.ES_PASSWORD;

	if (process.env.ES_HOST)
		host 		= process.env.ES_HOST;

	if (process.env.ES_PORT)
		port 		= process.env.ES_PORT;

	var url 		= username + ":" + password + "@" + host + ":" + port;

	client 			= new elasticsearch.Client( {
		host: url,
		log: 'debug' //error, warning, info, debug, trace
	});
};

const search = ( index, type, body, callback ) => {
	console.log('SEARCH:',  index, type, body);
	client.search({
	  index: index,
	  type: type,
	  body: body
	}, callback);
};


module.exports = {
	init: init,
	updateConfig: updateConfig,
	getRecords: getRecords
};

//sails.on('lifted', init);

