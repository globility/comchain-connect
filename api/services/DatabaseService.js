
var createData = function(params, callback) {
    Schedule.create(params).exec(function createCB(err,created){
        if (err) {
            console.log("CREATE ERROR");
            return callback(err);
        } else {
            return callback(created);
        }
    });
};

var findData = function(params, callback) {
    Schedule.find(params).exec(function findCB(err,created){
        if (err) {
            console.log("FIND ERROR");
            return callback(err);
        } else {
            return callback(created);
        }
    });
};

var editData = function(params, callback) {
    Schedule.update({
        id: params.id
    }, params).exec(function editCB(err,created){
        if (err) {
            console.log("EDIT ERROR", err);
            return callback(err);
        } else {
            console.log("CREATED:", created);
            return callback(err, created);
        }
    });
};

var completeData = function(params, callback) {
    Schedule.update(params, {
        processed: true
    }).exec(function completeCB(err,created){
        if (err) {
            console.log("COMPLETE ERROR");
            return callback(err);
        } else {
            return callback(created);
        }
    });
};

var deleteData = function(params, callback) {
    console.log("Deleting Database Row");
    Schedule.destroy({
        id: params.id
    }).exec(function deleteCB(err, created) {
        if (err) {
            console.log("DELETE ERROR");
            return callback(err, created);
        } else {
            return callback(err, created);
        }
    });
};

var getSettings = function(params, callback) {
    Settings.find().exec(function findCB(err,created){
        if (err) {
            console.log("FIND ERROR");
            return callback(err);
        } else {
            return callback("", created);
        }
    });
};

var createSettings = function(params, callback) {
    Settings.create(params).exec(function createCB(err,created){
        if (err) {
            console.log("CREATE ERROR");
            return callback(err);
        } else {
            return callback("", created);
        }
    });
};

var updateSettings = function(params, callback) {
    Settings.update({
        id: params.id
    }, params).exec(function createCB(err,created){
        if (err) {
            console.log("UPDATE ERROR");
            return callback(err);
        } else {
            return callback("", created);
        }
    });
};

module.exports = {
	createData: createData,
    findData: findData,
    editData: editData,
    deleteData: deleteData,
    completeData: completeData,
    getSettings: getSettings,
    createSettings: createSettings,
    updateSettings: updateSettings
};