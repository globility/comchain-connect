/**
* Schedule.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    name: {type: 'string'},
    jid : { type: 'string' },
    system : { type: 'string' },
    interest : { type: 'string' },
    users: { type: 'object' },
    groups: { type: 'object' },
    destination : { type: 'string' },
    time : { type: 'string' },
    year : { type: 'string' },
    repeat : { type: 'boolean' },
    repeatFrequency: { type: 'string'},
    processed : { type: 'boolean' },
    schedules: { type: 'object' }
    //     groups: { type: 'array' }, // An array of objects, each object being the group name and interest setting, default/all
    //     users: { type: 'array' } // An array of objects, each object being the user name and the interests or setting
    // }
  }
};

