/**
* Schedule.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    destination: {type: 'string'},
    messgaeLength: {type: 'string'},
    casHost : { type: 'string' },
    casPort : { type: 'string' },
    casHttps : { type: 'boolean' },
    nexusHost : { type: 'string' },
    nexusPort : { type: 'string' },
    nexusHttps : { type: 'boolean' },
    dataSourceHost : { type: 'string' },
    dataSourcePort : { type: 'string' }
  }
};

