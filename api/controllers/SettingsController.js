/**
 * ScheduleController
 *
 * @description :: Server-side logic for managing Schedules
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  /**
   * `ScheduleController.create()`
   */
  create: function (req, res) {
    var params = req.body;
    DatabaseService.createSettings(params, function(err, results) {
      console.log("CREATE db callback");
      if (err) {
        return res.serverError(err);
      }
      return res.json(results);
    });
  },

  /**
   * `ScheduleController.edit()`
   */
  update: function (req, res) {
    var params = req.body;
    DatabaseService.updateSettings(params, function(err, results) {
      console.log("EDIT db callback");
      if (err) {
        return res.serverError(err);
      }
      return res.json(results);
    })
  }
};

