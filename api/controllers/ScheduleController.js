/**
 * ScheduleController
 *
 * @description :: Server-side logic for managing Schedules
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  /**
   * `ScheduleController.index()`
   */
  index: function (req, res) {
    return res.json({
      todo: 'index() is not implemented yet!'
    });
  },

  /**
   * `ScheduleController.create()`
   */
  create: function (req, res) {
    var params = req.body;
    DatabaseService.createData(params, function(err, results) {
      console.log("CREATE db callback");
      if (err) {
        return res.serverError(err);
      }
      return res.json(results);
    });
  },

  /**
   * `ScheduleController.show()`
   */
  show: function (req, res) {
    return res.json({
      todo: 'show() is not implemented yet!'
    });
  },

  /**
   * `ScheduleController.edit()`
   */
  edit: function (req, res) {
    var params = req.body;
    DatabaseService.editData(params, function(err, results) {
      console.log("EDIT db callback");
      if (err) {
        return res.serverError(err);
      }
      return res.json(results);
    })
  },

  complete: function (req, res) {
    var params = req.body;
    DatabaseService.completeData(params, function(err, results) {
      console.log("COMPLETE db callback");
      if (err) {
        return res.serverError(err);
      }
      return res.json(results);
    })
  },

  /**
   * `ScheduleController.delete()`
   */
  delete: function (req, res) {
    var params = req.body;
    DatabaseService.deleteData(params, function(err, results) {
      console.log("DELETE db callback", err, results);
      if (err) {
        return res.serverError(err);
      }
      return res.json(results);
    });
  }
};

