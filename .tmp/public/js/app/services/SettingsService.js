'use strict';

rfbApp.factory('SettingsService', function($q, $http) {
    return {
        getSettings: function (data) {
            var defer = $q.defer();

            $http.get('/settings')
                .then(function (response) {
                    console.log("GET SETTINGS RESPONSE", response);
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    defer.reject('Unable to update settings: ' + err.toString());
                });

            return defer.promise;
        },
        updateSettings: function (data) {
            var defer = $q.defer();

            $http.post('/settings/update', data)
                .then(function (response) {
                    console.log("UPDATE SETTINGS RESPONSE", response);
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    defer.reject('Unable to update settings: ' + err.toString());
                });

            return defer.promise;
        }
    }
});