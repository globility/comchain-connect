'use strict';

rfbApp.factory('GraphService', function($q, $http) {
    return {
        getData: function(range) {
            var defer = $q.defer();

            $http.post('/mde', {
                range: range
            })
                .then(function (response) {
                    console.log("MDE RESPONSE", response);
                    defer.resolve(response.data.data);
                })
                .catch(function (err) {
                    defer.reject('Unable to get graph data: ' + err.toString());
                });

            return defer.promise;
        },
        getProviders: function() {
            var defer = $q.defer();
            console.log("get providers");
            $http.get('/providers')
                .then(function (response) {
                    console.log("PROVIDERS RESPONSE", response);
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    defer.reject('Unable to get provider data: ' + err.toString());
                });

            return defer.promise;
        },
        getCommunities: function() {
            var defer = $q.defer();
            console.log("get communities");
            $http.get('/communities')
                .then(function (response) {
                    console.log("COMMUNITIES RESPONSE", response);
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    defer.reject('Unable to get community data: ' + err.toString());
                });

            return defer.promise;
        },
        getCommunityConcat: function() {
            var defer = $q.defer();
            console.log("get community concat");
            $http.get('/communityConcat')
                .then(function (response) {
                    console.log("COMMUNITY CONCAT RESPONSE", response);
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    defer.reject('Unable to get community concat data: ' + err.toString());
                });

            return defer.promise;
        },
        getGroups: function() {
            var defer = $q.defer();
            console.log("get groups");
            $http.get('/groups')
                .then(function (response) {
                    console.log("GROUPS RESPONSE", response);
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    defer.reject('Unable to get group data: ' + err.toString());
                });

            return defer.promise;
        },
        getProducts: function() {
            var defer = $q.defer();
            console.log("get products");
            $http.get('/contracts')
                .then(function (response) {
                    console.log("PRODUCTS RESPONSE", response);
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    defer.reject('Unable to get product data: ' + err.toString());
                });

            return defer.promise;
        },
        getProductTemplate: function(productId) {
            var defer = $q.defer();

            $http.post('/contract/template', {
                id: productId
            })
                .then(function (response) {
                    console.log("PRODUCT TEMPLATE RESPONSE", response);
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    defer.reject('Unable to get product template data: ' + err.toString());
                });

            return defer.promise;
        },
        getUsers: function() {
            var defer = $q.defer();
            console.log("get users");
            $http.get('/users')
                .then(function (response) {
                    console.log("USERS RESPONSE", response);
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    defer.reject('Unable to get user data: ' + err.toString());
                });

            return defer.promise;
        },
        getMembers: function() {
            var defer = $q.defer();
            console.log("get members");
            $http.get('/members')
                .then(function (response) {
                    console.log("MEMBERS RESPONSE", response);
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    defer.reject('Unable to get member data: ' + err.toString());
                });

            return defer.promise;
        }
    }
});