'use strict';

rfbApp.controller('NavigationCtrl', function($scope, $rootScope, $location, $routeParams) {
    $scope.location = $location;
    $scope.params = $routeParams;
});