'use strict';

rfbApp.controller('SettingsCtrl',  function($scope, $rootScope, $location, $uibModal, SettingsService) {
    console.log("settings controller");
    if (!$scope.settings) {
        SettingsService.getSettings().then(function(response) {
            $scope.settings = response;
        });
    }
    $scope.open = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'templates/settingsModal.html',
            controller: 'ModalSettingsCtrl',
            controllerAs: '$ctrl'
        });

        modalInstance.result.then(function (data) {
            console.log("Modal Saved");
            SettingsService.updateSettings(data).then(function(response) {
              console.log("Settings Updated");
              $scope.settings = response;
            })
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
    };
});