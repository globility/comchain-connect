rfbApp.controller('ModalSettingsCtrl', function ($scope, $uibModalInstance, SettingsService) {
  var $ctrl = this;
  $ctrl.data = {};
  $ctrl.data.casHttps = false;
  $ctrl.data.nexusHttps = false;

  SettingsService.getSettings()
    .then(function(response) {
      if (response) {
        $ctrl.data = response;
      }
    });

  $ctrl.ok = function () {
    //console.log($ctrl.data);
    $uibModalInstance.close($ctrl.data);
  };

  $ctrl.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };

});