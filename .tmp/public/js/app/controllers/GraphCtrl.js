'use strict';

rfbApp.controller('DashboardCtrl',  function($scope, $rootScope, $location, $routeParams, $uibModal, GraphService) {
    $scope.table = {};
    $scope.productTable = {};
    $scope.summary = {};

    $scope.filterParam = {};

    $scope.range = {
        startDate: new Date(),
        endDate: new Date()
    };

    $scope.init = function() {
        $scope.getData();
        $scope.getCommunities();
        $scope.getCommunityConcat();
        $scope.getGroups();
        $scope.getMembers();
        $scope.getProducts();
        if ($scope.summary) {
            $scope.summary.open = true;
        }
        if ($scope.table) {
            $scope.table.open = true;
        }
        if ($scope.productTable) {
            $scope.productTable.open = true;
        }
        GraphService.getProviders()
            .then(function(response) {
                console.log(response)
                $scope.providers = response;
            })
    };

    $scope.getData = function() {
        console.log("GET GRAPH DATA");
        GraphService.getData($scope.range)
            .then(function(response) {
                $scope.transactions = response;
                $scope.filteredTransactions = _.clone($scope.transactions)
                console.log("GRAPH DATA", $scope.transactions);
                console.log("GRAPH DATA COPY", $scope.filteredTransactions);
                $scope.filterData('all');
            })
    };

    $scope.getCommunities = function() {
        console.log("GET COMMUNITY DATA");
        GraphService.getCommunities()
            .then(function(response) {
                $scope.communities = response;
                console.log("COMMUNITY DATA", $scope.communities);
            })
    };

    $scope.getCommunityConcat = function() {
        console.log("GET COMMUNITY CONCAT DATA");
        GraphService.getCommunityConcat()
            .then(function(response) {
                $scope.communityConcat = response;
                console.log("COMMUNITY CONCAT DATA", $scope.communityConcat);
            })
    };

    $scope.getGroups = function() {
        console.log("GET GROUP DATA");
        GraphService.getGroups()
            .then(function(response) {
                $scope.groups = response
                console.log("GROUP DATA", $scope.groups);
            })
    };

    $scope.getMembers = function() {
        console.log("GET MEMBER DATA");
        GraphService.getMembers()
            .then(function(response) {
                $scope.members = response
                console.log("MEMBER DATA", $scope.members);
            })
    };

    $scope.getProducts = function() {
        console.log("GET PRODUCT DATA");
        GraphService.getProducts()
            .then(function(response) {
                $scope.products = response;
                $scope.filteredProducts = _.clone($scope.products);
                console.log("PRODUCT DATA", $scope.products);
                GraphService.getUsers()
                    .then(function(response) {
                        $scope.users = response;
                        console.log("USER DATA", $scope.users);
                        $scope.filterData('all');
                    })
            })
    };

    $scope.filterData = function(type) {
        console.log("FILTER",$scope.filterParam);
        if ((type === 'community' || type === "all") && $scope.filterParam.community) {
            if (type !== 'all') {
                $scope.filterParam.group = '';
                $scope.filterParam.member = '';
            }
            var community = $scope.filterParam.community.id;

            //Groups
            $scope.filteredGroups = _.filter($scope.groups, function(group) {
                if (group.community === community) {
                    return true;
                }
            });
            console.log("GROUPS", $scope.filteredGroups);

            //Users
            $scope.filteredMembers = _.filter($scope.members, function(member) {
                if (member.community === community) {
                    return true;
                }
            });
            console.log("USERS", $scope.filteredMembers);

            //Products
            var communityUsers = $scope.communityConcat[community].members;
            console.log("COMMUNITY USERS", communityUsers);
            $scope.filteredProducts = _.filter($scope.products, function(product) {
                if (communityUsers) {
                    var owners = product.owners;
                    for (let owner of owners) {
                        if (_.contains(communityUsers, owner)) {
                            return true;
                        }
                    }
                }
            });
            console.log("PRODUCTS", $scope.filteredProducts);

            //Records
            if ($scope.transactions && $scope.transactions.records) {
                $scope.filteredTransactions.records = _.filter($scope.transactions.records, function(call) {
                    if (communityUsers) {
                        if (_.contains(communityUsers, call.owner.id)) {
                            return true;
                        }
                    }
                });
            }
            console.log("TRANSACTIONS", $scope.filteredTransactions);
        }

        if ((type === 'group' || type === "all") && $scope.filterParam.group) {
            var group = $scope.filterParam.group.id;
            if (type !== 'all') {
                $scope.filterParam.member = '';
            }

            //Users
            var groupUsers = [];
            $scope.filteredMembers = _.filter($scope.members, function(member) {
                if (_.contains(member.groups, group)) {
                    groupUsers.push(member.id);
                    return true;
                }
            });
            console.log("G-FILTER USERS", $scope.filteredMembers);

            //Products
            $scope.filteredProducts = _.filter($scope.products, function(product) {
                for (let owner of product.owners) {
                    if (_.contains(groupUsers, owner)) {
                        return true;
                    }
                }
            });
            console.log("G-FILTER PRODUCTS", $scope.filteredProducts);

            //Records
            if ($scope.transactions && $scope.transactions.records) {
                $scope.filteredTransactions.records = _.filter($scope.transactions.records, function(call) {
                    if (_.contains(groupUsers, call.owner.id)) {
                        return true;
                    }
                });
            }
            console.log("G-FILTER TRANSACTIONS", $scope.filteredTransactions);
        }

        if ((type === 'member' || type === "all") && $scope.filterParam.member) {
            var member = $scope.filterParam.member;

            //Groups
            $scope.filteredGroups = _.filter($scope.groups, function(group) {
                console.log("g filter group", group);
                if (_.contains(member.groups, group.id)) {
                    return true;
                }
            });
            console.log("M-FILTER GROUPS", $scope.filteredGroups);

            //Products
            $scope.filteredProducts = _.filter($scope.products, function(product) {
                if (_.contains(product.owners, member.id)) {
                    return true;
                }
            });
            console.log("M-FILTER PRODUCTS", $scope.filteredProducts);

            //Records
            if ($scope.transactions && $scope.transactions.records) {
                $scope.filteredTransactions.records = _.filter($scope.transactions.records, function(call) {
                    if (call.owner.id === member.id) {
                        return true;
                    }
                });
            }
            console.log("M-FILTER TRANSACTIONS", $scope.filteredTransactions);
        }
    }

    $scope.resetDate = function(type) {
        if ($routeParams.type === "eod") {
            var today = new Date();
            var yesterday = new Date(today.setDate(today.getDate() - 1));
            $scope.range.startDate = yesterday;
            $scope.range.endDate = yesterday;
            console.log("eod route");
        } else {
            $scope.range.startDate = new Date();
            $scope.range.endDate = new Date();
        }
        $scope.getData();
    }

    $scope.popup1 = {
        opened: false
    };
    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };
    $scope.popup2 = {
        opened: false
    };
    $scope.open2 = function() {
        $scope.popup2.opened = true;
    };

    $scope.startOptions = {
        maxDate: $scope.range.endDate,
        showWeeks: true
    };
    $scope.$watch('range.endDate', function() {
        $scope.startOptions.maxDate = $scope.range.endDate;
    });

    $scope.endOptions = {
        maxDate: new Date(),
        minDate: $scope.range.startDate,
        showWeeks: true
    };
    $scope.$watch('range.startDate', function() {
        $scope.endOptions.minDate = $scope.range.startDate;
    });

    $scope.saveStaticFilter = function() {
        console.log($scope.query);
        $scope.getData();
    }

    $scope.getCol = function( call ){
        
        var cols = [];

        try {
            var interest           = _.findWhere(call.metadata, { name: 'interestid' }).interestid;
            cols.push({title: 'Interest', label: interest});
        } catch(er){
            cols.push({title: 'Interest', label: ''});
        }

        try {
            var starttimestamp           = _.findWhere(call.metadata, { name: 'starttimestamp' }).starttimestamp;
            cols.push({title: 'Starttimestamp', label: starttimestamp});
        } catch(er){
            cols.push({title: 'Starttimestamp', label: ''});
        }

        try {
            var endtimestamp           = _.findWhere(call.metadata, { name: 'endtimestamp' }).endtimestamp;
            cols.push({title: 'Endtimestamp', label: endtimestamp});
        } catch(er){
            cols.push({title: 'Endtimestamp', label: ''});
        }

        return cols;
    };

    $scope.findLabel = function( column, call ) {
        try {
            if (column === 'owner' && call.owner) {
                return call.owner.name;
            } else if (column === 'types') {
                return call.types.join(', ');
            } else {
                return _.findWhere(call.metadata, {name: column})[column];
            }
        } catch (error) {
            return 'N/A';
        }
    };

    $scope.verify = function() {
        console.log('setting timeout');
        window.setTimeout(function() {
            console.log('timeout running');
            for (let call of $scope.transactions.records) {
                if (call.isValid) {
                    call.validity = true;
                } else {
                    call.validity = false;
                }
                $scope.validity = true;
            }
            $scope.$apply();
        }, 2000);
    };

    $scope.resetFilter = function() {
        $scope.query = {};
    };

    $scope.open = function (record) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'templates/graphBreakdownModal.html',
            controller: 'ModalGraphBreakdownCtrl',
            controllerAs: '$ctrl',
            size: 'lg',
            resolve: {
                records: function() {
                    return $scope.transactions.records;
                },
                record: function () {
                    return record;
                },
                providers: function () {
                    return $scope.providers;
                }
            }
        });

        modalInstance.result.then(function (data) {
          console.log("MODAL DISMISSED", data);
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.openTemplate = function (product) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'templates/productTemplateModal.html',
            controller: 'ModalProductTemplateCtrl',
            controllerAs: '$ctrl',
            resolve: {
                product: function () {
                    return product;
                },
                providers: function () {
                    return $scope.providers;
                }
            }
        });

        modalInstance.result.then(function (data) {
          console.log("MODAL DISMISSED", data);
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.verifyOpen = function (record) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'templates/hashVerifyModal.html',
            controller: 'ModalHashCtrl',
            controllerAs: '$ctrl',
            size: 'lg',
            resolve: {
                record: function () {
                    return record;
                }
            }
        });

        modalInstance.result.then(function (data) {
          console.log("MODAL DISMISSED", data);
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.verifySourceOpen = function (source, type) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'templates/hashVerifySourceModal.html',
            controller: 'ModalSourceHashCtrl',
            controllerAs: '$ctrl',
            resolve: {
                data: function () {
                    return $scope.transactions[type];
                },
                source: function() {
                    return source;
                }
            }
        });

        modalInstance.result.then(function (data) {
          console.log("MODAL DISMISSED", data);
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.init();
});