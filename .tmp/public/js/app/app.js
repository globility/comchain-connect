'use strict';

var rfbApp = angular.module('rfbApp', ['ngRoute', 'ui.bootstrap', 'ngSanitize', 'ui.select', 'chart.js']);

rfbApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: '/templates/graphs.html'
    });

    // $routeProvider.when('/schedule/add', {
    //     templateUrl: '/templates/schedule.html'
    // });

    // $routeProvider.when('/schedule/:jobId', {
    //     templateUrl: '/templates/schedule.html'
    // });

    // $routeProvider.when('/graphs/:type', {
    //     templateUrl: '/templates/graphs.html'
    // });

    $routeProvider.otherwise({
        redirectTo: '/'
    });
}]);