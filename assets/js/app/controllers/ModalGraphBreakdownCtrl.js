rfbApp.controller('ModalGraphBreakdownCtrl', function ($scope, $uibModal, $uibModalInstance, records, record, providers) {
  var $ctrl = this;
  $ctrl.records = records;
  console.log($ctrl.records);
  $ctrl.record = record;
  $ctrl.metadataHash = _.findWhere($ctrl.record.metadata, {name: 'filehash'});
  console.log($ctrl.record);
  $ctrl.providers = providers;
  $ctrl.extraParticipants = [];

  $ctrl.openLinked = function(linkedId) {
    console.log("linked id", linkedId);
    var newRecord = _.findWhere($ctrl.records, {id: linkedId});
    console.log("new records", newRecord);
    $ctrl.open(newRecord);
  };

  $ctrl.viewExtraParticipants = function(linkedId) {
    console.log("linked id", linkedId);
    var newRecord = _.findWhere($ctrl.records, {id: linkedId});
    console.log("new records", newRecord);
    var participants = _.findWhere(newRecord.metadata, {name: "participants"}).participants;
    for (var i = participants.length - 1; i >= 0; i--) {
      var participant = participants[i];
      console.log(participant);
      $ctrl.extraParticipants.push(participant.id);
    };
    $ctrl.extraParticipants = _.uniq($ctrl.extraParticipants);
  }

  $ctrl.open = function (record) {
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'templates/graphBreakdownModal.html',
      controller: 'ModalGraphBreakdownCtrl',
      controllerAs: '$ctrl',
      size: 'lg',
      resolve: {
          records: function() {
              return $ctrl.records;
          },
          record: function () {
              return record;
          },
          providers: function () {
              return $ctrl.providers;
          }
      }
    });

    modalInstance.result.then(function (data) {
      console.log("MODAL DISMISSED", data);
    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  };

  $ctrl.init = function () {
    $ctrl.metadata = {};
    var metadata = $ctrl.record.metadata;
    for (var _i=0;_i<metadata.length;_i++) {
      var data = metadata[_i];
      if (!$ctrl.metadata[data.provider]) {
        $ctrl.metadata[data.provider] = [];
        $ctrl.metadata[data.provider].push(data);
      } else {
        $ctrl.metadata[data.provider].push(data);
      }
    }
    console.log($ctrl.metadata);
  };

  $ctrl.init();

  $ctrl.ok = function (target) {
    $uibModalInstance.close({
    });
  };

  $ctrl.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});