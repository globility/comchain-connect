rfbApp.controller('ModalHashCtrl', function ($scope, $uibModalInstance, record) {
  var $ctrl = this;
  $ctrl.record = record;
  $ctrl.metadataHash = _.findWhere($ctrl.record.metadata, {name: 'filehash'});
  console.log("HASH RECORD", $ctrl.record);
  console.log("METADATA HASH", $ctrl.metadataHash);

  $ctrl.ok = function (target) {
    $uibModalInstance.close({
    });
  };

  $ctrl.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});