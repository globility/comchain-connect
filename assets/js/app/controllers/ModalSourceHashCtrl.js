rfbApp.controller('ModalSourceHashCtrl', function ($scope, $uibModalInstance, data, source) {
  var $ctrl = this;
  $ctrl.data = data;
  $ctrl.source = source;

  $ctrl.ok = function (target) {
    $uibModalInstance.close({
    });
  };

  $ctrl.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});