rfbApp.controller('ModalProductTemplateCtrl', function ($scope, $uibModalInstance, GraphService, product, providers) {
  var $ctrl = this;
  $ctrl.product = product;
  $ctrl.providers = providers;
  console.log("PROVIDERS", $ctrl.providers);

  $ctrl.init = function() {
    console.log($ctrl.product);
    GraphService.getProductTemplate($ctrl.product.comChain['@id'])
      .then(function(body) {
        var response = JSON.parse(body);
        console.log("template response", response);
        var templateArray = {};
        var keyArray = _.keys(response);
        var valueArray = _.values(response);
        console.log(keyArray, valueArray);
        for (var _i=0;_i<keyArray.length;_i++) {
          var key = keyArray[_i];
          var value = valueArray[_i];
          if (Array.isArray(value)) {
            value = $ctrl.providers[value[0]].name
            if (!templateArray[value]) {
              templateArray[value] = [];
              templateArray[value].push(key);
            } else {
              templateArray[value].push(key);
            }
          }
        }
        console.log(templateArray);
        $ctrl.template = templateArray;
      });
  };

  $ctrl.ok = function (target) {
    $uibModalInstance.close({
    });
  };

  $ctrl.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };

  $ctrl.init();
});